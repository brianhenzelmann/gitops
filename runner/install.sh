# Run in project root:
k create ns gitlab-runner --dry-run=client -oyaml > ./manifests/runner.yaml
echo "---" >> ./manifests/runner.yaml
helm template --namespace gitlab-runner gitlab-runner -f runner/runner-chart-values.yaml gitlab/gitlab-runner | k -n gitlab-runner apply --dry-run=client -oyaml -f - >> ./manifests/runner.yaml
